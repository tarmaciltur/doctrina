/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

public class Doctrina.Infraestructura : Object {
  private string nombre;
  private string version;
  private string db_resource;

  public Infraestructura ( string nombre, string version, string db_resource ) {
    this.nombre = nombre;
    this.version = version;
    this.db_resource = db_resource;
  }

  public static void crear_directorio ( string path ) {
    var directorio = File.new_for_path ( path );
    try {
      directorio.make_directory_with_parents ();
    }  catch ( Error e ) {
      error ( e.message );
    }
  }

  public static bool existe_path ( string path ) {
    var archivo = File.new_for_path ( path );
    var retorno = archivo.query_exists ();

    return retorno;
  }

  public string directorio_de_configuracion () {
    var config_dir = GLib.Environment.get_user_config_dir () + "/" + this.nombre;

    if ( !(Infraestructura.existe_path ( config_dir ) ) ) {
      Infraestructura.crear_directorio ( config_dir );
    }

    return config_dir;
  }

  public string version_db () {
    return this.version;
  }

  public string definicion_db () {
    string definicion_db = "";
    uint8[] contenido;
    string etag_out;


    var file_definicion_db = File.new_for_uri ( this.db_resource );

    try {
      file_definicion_db.load_contents ( null, out contenido, out etag_out );
    } catch ( GLib.Error e ) {
      stderr.printf ( "%s\n", e.message );
    }

    definicion_db = (string)contenido;

    return definicion_db;
  }
}
