/* -*- Mode: Vala; indent-tabs-mode: nil; c-basic-offset: 2; tab-width: 2 -*-  */

using Sqlite;
using Doctrina;

public class Doctrina.BaseDeDatos : Object {
  private Database db;
  private string filename;
  private Infraestructura infra;

  public BaseDeDatos ( string filename, Infraestructura infra ) {
    this.filename = filename;
    this.infra = infra;
    this.inicializar ();
  }

  private void inicializar () {
    this.db = open ();
    if ( this.db != null ) {
      if ( this.db.exec ( "SELECT * FROM db", null, null ) != Sqlite.OK ) {
        this.crear_tablas ();
      } else {
        this.comprobar_version ();
      }
    }
  }

  private Database open () {
    Database db;
    int result_code;

    var archivo_db = File.new_for_path ( this.archivo_db () );

    if ( !archivo_db.query_exists () ) {
      this.crear_archivo_db ();
    }

    while ( Database.open ( this.archivo_db (), out db ) != Sqlite.OK ) {
      stderr.printf ( ( "Could not open the data base") + ": %s\n", db.errmsg () );
      if ( !this.crear_archivo_db () ) {
        break;
      }
    }

    if ( db != null ) {
      result_code = db.exec ( "PRAGMA foreign_keys=ON;", null, null );

      if ( result_code != Sqlite.OK ) {
        stderr.printf ( "SQL error: %d, %s\n", result_code, db.errmsg () );
      }
    }

    return db;
  }

  private bool crear_archivo_db () {
    bool retorno = true;

    if ( retorno != false ) {
      var archivo_db = File.new_for_path ( this.archivo_db () );
      try {
        archivo_db.create ( FileCreateFlags.PRIVATE );
      } catch ( Error e ) {
        stderr.printf ( "Error: %s\n", e.message );
        retorno = false;
      }
    }

    return retorno;
  }

  private bool crear_tablas () {
    bool retorno = true;
    string definicion_db;
    string mensaje_error = "";

    definicion_db = this.infra.definicion_db ();

    debug ( "inicializando talbas de la db" );
    var result_code = db.exec ( definicion_db, null, out mensaje_error );

    if ( result_code != Sqlite.OK ) {
      retorno = false;
      debug ( "Error al cargar la definición de la db: %s", mensaje_error );
    }

    this.insert ( "db", "version", this.infra.version_db () );
    return retorno;
  }

  private void comprobar_version () {
    var version = this.select ( "db", "version", "" ).index ( 0 );
    debug ( "version del archivo de la db: " + version );
    debug ( "version del archivo de la infraestructura: " + this.infra.version_db () );

    if ( version != this.infra.version_db () ) {
      debug ( "la version del archivo de la db y de la infrastrucrura difieren" );
      if ( this.backup_db (  this.infra.version_db () ) ) {
        this.inicializar ();
      }
    }
  }

  private bool backup_db ( string version ) {
    bool retorno = true;
    var archivo_db = File.new_for_path ( this.archivo_db () );
    var archivo_backup = File.new_for_path ( this.archivo_db () + "." + version + ".bak" );
    debug ( "creando backup del archivo de la db" );
    try {
      archivo_db.move ( archivo_backup, FileCopyFlags.NONE, null, null );
    } catch ( Error e ) {
      stderr.printf ( "%s\n", e.message );
      retorno = false;
    }

    return retorno;
  }

  private bool query ( string sql_query, out Statement stmt ) {
    bool retorno = false;

    stmt = null;

    var result_code = db.prepare_v2 ( sql_query, -1, out stmt, null );

    debug ( "ejecutando query: %s", sql_query );

    if ( result_code != Sqlite.OK ) {
      stderr.printf ( sql_query );
      stderr.printf ( ("Failed to execute the sentence\n") + ": %s - %d - %s", sql_query, result_code, db.errmsg () );
    } else {
      retorno = true;
    }

    return retorno;
  }

  public bool insert ( string tabla, string columnas, string valores ) {
    bool retorno = false;

    string query = "INSERT INTO \""+ tabla +"\" (" + columnas + ") VALUES (" + valores + ")";
    var result_code = db.exec ( query, null, null );

    debug ( "ejecutando query: %s", query );

    if ( result_code != Sqlite.OK ) {
      stderr.printf ( query + "\n" );
      stderr.printf ( "SQL error: %d, %s\n", result_code, db.errmsg () );
    } else {
      retorno = true;
    }

    return retorno;
  }

  public bool del ( string tabla, string where = "" ) {
    bool retorno = false;

    string query = "DELETE FROM " + tabla + " " + where;

    var result_code = db.exec ( query, null, null );

    debug ( "ejecutando query: %s", query );

    if ( result_code != Sqlite.OK ) {
      stderr.printf ( query + "\n" );
      stderr.printf ( "SQL error: %d, %s\n", result_code, db.errmsg () );
    } else {
      retorno = true;
    }

    return retorno;
  }

  public bool update ( string tabla, Array<string> valores, string condicion ) {
    bool retorno = false;
    string datos = "";

    var where = "WHERE " + condicion;

    for ( int i = 0; i < valores.length; i++ ) {
      datos += valores.index ( i );

      if ( i != (valores.length - 1) ) {
        datos += ", ";
      }
    }

    string query = "UPDATE " + tabla + " SET " + datos + " " + where;

    var result_code = db.exec ( query, null, null );

    debug ( "ejecutando query: %s", query );

    if ( result_code != Sqlite.OK ) {
      stderr.printf ( query + "\n" );
      stderr.printf ( "SQL error: %d, %s\n", result_code, db.errmsg () );
    } else {
      retorno = true;
    }

    return retorno;
  }

  public Array<string> select ( string tabla, string columnas, string where = "" ) {
    Statement stmt;
    Array<string> resultado_de_la_consulta = new Array<string> ();


    this.query ( "SELECT " + columnas + " FROM " + tabla + " " + where, out stmt );

    resultado_de_la_consulta = this.parse_query ( stmt );

    return resultado_de_la_consulta;
  }

  public Array<string> select_distinct ( string tabla, string columnas, string where = "" ) {
    Statement stmt;
    Array<string> resultado_de_la_consulta = new Array<string> ();

    this.query ( "SELECT DISTINCT " + columnas + " FROM " + tabla + " " + where, out stmt );

    resultado_de_la_consulta = this.parse_query ( stmt );

    return resultado_de_la_consulta;
  }

  public Array<string> count ( string tabla, string where ) {
    Statement stmt;
    Array<string> resultado_de_la_consulta = new Array<string> ();

    this.query ( "SELECT COUNT (*) FROM " + tabla + " " + where, out stmt );

    resultado_de_la_consulta = this.parse_query ( stmt );

    return resultado_de_la_consulta;
  }

  private Array<string> parse_query ( Statement stmt ) {
    Array<string> tuplas = new Array<string> ();
    string tupla;
    string campo;

    int cols = stmt.column_count ();
    int rc = stmt.step ();

    while ( rc == Sqlite.ROW ) {
      switch ( rc  ) {
        case Sqlite.DONE:
          break;
        case Sqlite.ROW:
          tupla = "";
          for ( int i = 0; i < cols; i++ ) {
            campo = stmt.column_text ( i );
            tupla += campo;
            if ( i < cols-1 ) {
              tupla += "|";
            }
          }
          tuplas.append_val ( tupla );
          break;
        default:
          print ( "Error parseando la query" );
          break;
      }

      rc = stmt.step ();
    }

    return tuplas;
  }

  public string archivo_db () {
    return this.infra.directorio_de_configuracion () + "/" + this.filename;
  }

  public void begin_transaction () {
    Statement stmt;
    this.query ( "BEGIN TRANSACTION;", out stmt );
  }

  public void commit_transaction () {
    Statement stmt;
    this.query ( "COMMIT;", out stmt );
  }

  public void rollback_transaction () {
    Statement stmt;
    this.query ( "ROLLBACK;", out stmt );
  }

  public string ultimo_id ( string tabla ) {
    return this.select ( "sqlite_sequence", "seq", "WHERE name='" + tabla + "'" ).index ( 0 );
  }
}
